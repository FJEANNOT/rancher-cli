FROM ubuntu:latest

WORKDIR /home
ADD https://github.com/rancher/cli/releases/download/v2.3.2/rancher-linux-amd64-v2.3.2.tar.gz /home
RUN tar -xvf rancher-linux-amd64-v2.3.2.tar.gz\
    && chmod +x rancher-v2.3.2/rancher\
    && cp rancher-v2.3.2/rancher /bin/rancher

ADD https://get.helm.sh/helm-v3.0.3-linux-amd64.tar.gz /home
RUN tar xvf helm-v3.0.3-linux-amd64.tar.gz\
    && chmod +x linux-amd64/helm\
    && cp linux-amd64/helm /bin/helm
